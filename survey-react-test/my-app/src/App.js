

import React, { useEffect } from 'react'
import * as Survey from "survey-knockout";
import * as SurveyCreator from "survey-creator";


import 'bootstrap/dist/css/bootstrap.css';
import "survey-knockout/survey.css";
import "survey-creator/survey-creator.css";
import "./index.css";

SurveyCreator.StylesManager.applyTheme("darkblue");


const App = () => {
  useEffect(() => {

    
    // Show Designer, Test Survey, JSON Editor and additionally Logic tabs
    var options = {
      showLogicTab: true,
      isAutoSave: true,
      showTestSurveyTab: true,
      showLogicTab : false,
      showJSONEditorTab : false,
      questionTypes: ["text", "checkbox", "radiogroup"]
    };
    //create the SurveyJS Creator and render it in div with id equals to "creatorElement"
    var creator = new SurveyCreator.SurveyCreator("creatorElement", options);
    var localStorageName = "SaveLoadSurveyCreatorExample";
    //Setting this callback will make visible the "Save" button
    creator.saveSurveyFunc = function (saveNo, callback) {
      //save the survey JSON
      console.log(creator.text);
      //You can store in your database JSON as text: creator.text  or as JSON: creator.JSON
      window
        .localStorage
        .setItem(localStorageName, creator.text);

      //We assume that we can't get error on saving data in local storage
      //Tells creator that changing (saveNo) saved successfully.
      //Creator will update the status from Saving to saved
      callback(saveNo, true);
    }
    //Show toolbox in the right container. It is shown on the left by default
    creator.showToolbox = "right";
    //Show property grid in the right container, combined with toolbox
    creator.showPropertyGrid = "right";
    //Make toolbox active by default
    creator.rightContainerActiveItem("toolbox");
    creator.toolbarItems.push({
      id: "clear-survey",
      visible: true,
      title: "Clear Survey",
      action: function () {
        //Clear survey
        creator.JSON = {};
        window
          .localStorage.removeItem(localStorageName)
      }
    },
      {
        id: "save-survey",
        visible: true,
        title: "Save Survey To Server",
        action: function () {
          // //Clear survey
          // creator.JSON = {};
        }
      });

    var defaultJSON = {
      pages: [

      ]
    };
    creator.text = window
      .localStorage
      .getItem(localStorageName) || JSON.stringify(defaultJSON);


  })
  return (
    <div>
      <div id="creatorElement" />
    </div>
  )
}

export default App
